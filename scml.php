<?php
/*
Plugin Name: Shortcode Markup Language
Description: Shortcodes for common GDA.net HTML
Version: 0.1alpha
Author: Herb Miller Jr.
*/

function fark()
{
	return exec('/mnt/www/test.cgi');
}
add_shortcode('fark','fark');

/* ** Disclaimers ** *\
|* Various messages that bark legal stuff at people.
\* ** */
function disclaimer_exclusive_content()
{
	return '<br>~ ---- ~<br>All exclusive content on GDA.net is perpetually in the public domain, however if any GDA.net branded content (name or logo) is used in another production, please cite the author, Herb Miller Jr., or the source, greatdayapproaches.net. Otherwise I will be inclined to add to my administrative overhead by charging you a beer.';
}
add_shortcode('dc_ec','disclaimer_exclusive_content');

/* ** Formatting ** *\
|* General fonts, decorations, and containers designed to match the site theme.
\* ** */

function caption($atts,$content=null)
{
	extract(
		shortcode_atts(
			array(
				'title' => '',
				'spacing' => '0em',
			),
		$atts)
	);
	//$retval='<div style="margin: 0px; padding: 0px; border: solid silver 1px;">';
	$retval='<div style="margin: 0px; padding: 0px; border: solid silver 1px; margin-top: ';
	$retval.=$spacing;
	$retval.=';">';
	$retval.='<div style="width: 90%; text-align: center; padding: 5%;">';
	$retval.=$content;
	$retval.='</div>';
	$retval.='<div style="background-color: white; text-align: center; padding: 0.5em; border-top: solid silver 1px;">';
	$retval.=$title;
	$retval.='</div></div>';
	return $retval;
}
add_shortcode('gallery_caption','caption');

function superscript($atts,$content=null)
{
	return '<span style="font-size:50%;margin-top:-1.5em;">'.do_shortcode($content).'</span>';
}
add_shortcode('sup','superscript');

function codeblock($atts,$content=null)
{
	extract(
		shortcode_atts(
			array(
				'description' => '',
				'margin_bottom' => '0.5em',
			),
		$atts)
	);
	$retval='';
	if ($description!='')
	{
		$retval='<p style="padding: 0em 2em 0em 0em; margin: 0px; background-color: #cccccc; font-weight: bold; text-align: right;">'.$description.'</p>';
	}
	return $retval.'<p style="padding: 1em; margin-bottom: '.$margin_bottom.'; border: solid #cccccc 1px; font-family: monospace;  background-color: #ffffff;">'.$content.'</p>';
}
add_shortcode('codeblock','codeblock');

function jump_divider($atts,$contents=null)
{
	return '<div style="background-color:#cccccc; padding: 0px;"><span style="background-color:#ffffff; padding:0em 0.5em 0em 0.5em; margin:0em 0em 0em 1em;font-weight:bold;">'.$contents.'</span></div>';
}
add_shortcode('jump_divider','jump_divider');

// **
// -----=== Date selection ===-----
// **
function dropdown_month($atts)
{
	if ($atts["label"]) {$dropdownlist=$atts["label"];} else {$dropdownlist="";}
	$dropdownlist.='<select name="month">';
	for ($i=1;$i<=12;$i++)
	{
		$dropdownlist.='<option value="'.str_pad($i,2,"0",STR_PAD_LEFT).'"';
		if ($i==date("n")) {$dropdownlist.=' selected="selected"';}
		$dropdownlist.='>';
		
		switch ($i)
		{
			case 1:
				$dropdownlist.="January";
				break;
			case 2:
				$dropdownlist.="February";
				break;
			case 3:
				$dropdownlist.="March";
				break;
			case 4:
				$dropdownlist.="April";
				break;
			case 5:
				$dropdownlist.="May";
				break;
			case 6:
				$dropdownlist.="June";
				break;
			case 7:
				$dropdownlist.="July";
				break;
			case 8:
				$dropdownlist.="August";
				break;
			case 9:
				$dropdownlist.="September";
				break;
			case 10:
				$dropdownlist.="October";
				break;
			case 11:
				$dropdownlist.="November";
				break;
			case 12:
				$dropdownlist.="December";
				break;
		}
		
		$dropdownlist.='</option>';
	}
	
	$dropdownlist.='</select>';
	return $dropdownlist;		
}
add_shortcode('dropdown_month','dropdown_month');

function dropdown_day($atts)
{
	return '<script "text/javascript">document.write("testing");</script"';
}
add_shortcode('dropdown_day','dropdown_day');

function dropdown_year($atts)
{
	return '<select name="year" value='.'date("Y")'.'>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
		</select>';
}
add_shortcode('dropdown_year','dropdown_year');

/*****\
|	Formats code inline colored in blue.
\*****/
function monocode($atts,$content=null)
{
	return '<span style="font-family: monospace; color: #0000ff;">'.$content.'</span>';
}
add_shortcode('monocode','monocode');

/*****\
|	Formats code in an isolated block in monospace font but nothing more.
\*****/
function minicodeblock($atts,$content=null)
{
	return '<div style="background-color: #ffffff;padding: 0.5em;border:solid #cccccc 1px;font-family:monospace">'.$content.'</div>';
}
add_shortcode('minicodeblock','minicodeblock');

/*****\
|	Readjusts font to a more readable size and family.
\*****/
function reformat_readable($atts,$content=null)
{	
	extract(
		shortcode_atts(
			array(
				'indent' => '0em'
			),
		$atts)
	);
	return '<div style="font-size: 1.25em; font-family: serif; text-indent: '.$indent.';">'.do_shortcode($content).'</div>';
}
add_shortcode('reformat_readable','reformat_readable');

/*****\
|	Mark solution as you would on a forum.
\*****/
function formatted_tag($atts)
{
	extract(shortcode_atts(array('type'=>''),$atts));

	$retval='-undef-';
	$color='red';
	switch($type)
	{
		case 'fail':
			$retval='Fail';
			$color='red';
			break;
		case 'pass':
			$retval='Pass';
			$color='green';
			break;
		case 'solved':
			$retval='Solved';
			$color='green';
			break;
	}

	return '<span style="color: '.$color.'; font-variant:small-caps; font-weight: 550;">'.$retval.': </span>';
}
add_shortcode('formatted_tag','formatted_tag');

/*****\
|	Oh no! It's root! Make sure we make it look EVIL!
\*****/
function root($atts)
{
	return '<span style="color: #ff0000; font-weight: bold;">root</span>';
}
add_shortcode('root','root');

?>
